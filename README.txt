
-- SUMMARY --

The BMI Add Ons module is a sandbox project that provides additional blocks to the BMI module by Azri solutions (http://drupal.org/user/115278). These blocks offer additional tools to aid users is setting weight loss goals.

The BMI Module is a dependency

It adds a number of blocks that contain forms that help to calculate various health indicators.

In addition to adding a BMI calculator geared towards users of the imperial system of measurement,
the BMI Add Ons module provides the following for both users of the imperial and metric systems:
  - Basal Metabolic Rate (BMR) block (both imperial and metric)
  - Harris Benedict Formula block (applies to all users)
  - Body Fat Calculator block (both imperial and metric)
  - Waist to Hip Ratio Calculator block (both imperial and metric)
  
-- REQUIREMENTS --

BMI Module https://www.drupal.org/project/bmi


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Optional

  - This module adds a couple of settings to the BMI Settings Page

    Since the Basal Metabolic Rate (BMR) form(s) and Harris Benedict Formula form
    rely upon each other, you can add the page paths where you have placed these blocks
    in the BMI Settings. This will enable users to easily navigate from one form to another
    if they need to. In the case of the BMR form, this will allow users to link to the 
    Harris Benedict Formula form from the results of the BMR form, sending the required BRM result
    to the Harris Benedict Formula form by way of a query string, thereby improving user experience.

-- SUGGESTED --

You may optionally use the Quick Tabs Module (https://www.drupal.org/project/quicktabs) to join the imperial and metric versions of each block in a separate tab. This will enable your users to easily choose their form of preference, and will enable you to choose the default, in order to target   your primary user group. 

-- AUTHOR --

* John Lawter - https://www.drupal.org/u/john-lawter



